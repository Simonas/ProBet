﻿using ProBet.Core.Services;
using ProBet.Services;
using ProBet.Views;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using ProBet.Review;

namespace ProBet
{
    class Bootstrap : UnityBootstrapper
    {
        protected override void InitializeShell()
        {
            Application.Current.MainWindow = Shell as Window;
            Application.Current.MainWindow.Show();
        }

        protected override DependencyObject CreateShell()
        {
            return ServiceLocator.Current.GetInstance<ShellView>();
        }

        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();


            this.ModuleCatalog.AddModule(new ModuleInfo("ReviewModule", typeof(ReviewModule).AssemblyQualifiedName));

        }

        protected override void ConfigureContainer()
        {
            this.Container.RegisterType<ILogService, LogService>(new ContainerControlledLifetimeManager());
            //this.Container.RegisterType<IMenuService, ShellView>(new ContainerControlledLifetimeManager());
            this.Container.RegisterType<INotificationService, ShellView>(new ContainerControlledLifetimeManager());

            base.ConfigureContainer();
        }
    }
}
