﻿Užduotys:
1)	Susikurti Bootstrapa, kuriame sudeti modulius. Kolkas naudosim 1 modulį, bet vistiek dėkim, poto lengviau dirbti bus.
2)	Bootstrape uzregistruojam savo pagrindini View’a. Čia šitas mum padės kai norėsim back mygtuką naudoti ir pnš.
3)	Susikuriam pagrindinį vaizdą (vadinsim ShellView). Čia bus meniu, settings ir t.t.

	1,2,3 aš padarysiu.

4)	Sukurti duomenų bazę. Sukurti lentelę pavadinimu “LanguageTranslations” auto increment, primary key. 
	Joje turi būtu stulpelis “key” (string) ir stuleplis “LT” (string). Visus žodžius ar tekstus dėsime į šitą lentelę. Sukursim klasę, kuri parinks mum ką reikia.
5)	Reikia sukurti katalogą “Resources” ir jame sukurti klasę “StepView.cs”. Kolkas joje nieko nedaryk.
6)	Reikia sukurti katalogą “ViewModels”, jame sukurti “StepVMl.cs”. Šita klasė bus mūsų pagrindinė, ją paveldės kiti langai.
7)	“ViewModels” kataloge sukurti “Steps” katalogą, o jame sukurit klasę “ReviewVM.cs”. “review” bus mūsų pirmo tabo itemas.

	Padaryk 4,5,6,7.

Kai situos padarysim, pradesim programinti. Tai iš principo tau reiktų manes laukti, ir tik tada padaryti užduotis. 
