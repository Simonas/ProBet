﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Resources 
{
    public class Strings : TranslationResource
    {
        public static string MainRegion => "MainRegion";

        public static string NavigationRegion => "NavigationRegion";

        public static string StatusRegion => "StatusRegion";

        public static string FlyoutRegion => "FlyoutRegion";
        public static string Settings => _("title.settings");
        public static string AppName => _("title.appname");
        public static string Review => _("label.review");
        public static string Bets => _("label.bets");
    }
}
