﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProBet.Resources
{
    public abstract class TranslationResource
    {
        private static string _currentLanguage;
        private static Dictionary<string, string> _cache;


        protected static string _([ProBet.Properties.NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            if ((bool)(DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue))
            {
                return key;
            }


            return GetValue(key);
        }

        public static string GetValue(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            if (_currentLanguage == null)
            {
                _currentLanguage = "LT";
            }

            if (_cache == null)
            {
                _cache = new Dictionary<string, string>();
            }

            if (_cache.ContainsKey(key))
            {
                return _cache[key];
            }

            string translation = null;

            using (var dbConnection = new SQLiteConnection(ProBet.Core.Resources.CoreModule.DatabaseName))
            {

                var commandText = $"SELECT {_currentLanguage} FROM LanguagesTranslation where key='{key}'";

                var command = dbConnection.CreateCommand(commandText);

                translation = command.ExecuteScalar<string>();
                if (translation == null)
                {
                    commandText = $"SELECT {"LT"} FROM LanguageTranslation where key='{key}'";

                    command = dbConnection.CreateCommand(commandText);

                    translation = command.ExecuteScalar<string>();
                }
            }

            if (translation != null)
            {
                _cache.Add(key, translation);
                return translation;
            }
            return key;
        }
    }
}
