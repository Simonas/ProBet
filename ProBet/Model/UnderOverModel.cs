﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCode.Model
{
    public class UnderOverModel
    {
        public double HomeGoalsScored { get; set; }
        public double HomeGoalsConcended { get; set; }
        public double AwayGoalsScored { get; set; }
        public double AwayGoalsConcended { get; set; }

        public UnderOverModel(double hScored, double hConcended, double AScored, double AConcended)
        {
            this.HomeGoalsScored = hScored;
            this.HomeGoalsConcended = hConcended;
            this.AwayGoalsScored = AScored;
            this.AwayGoalsConcended = AConcended;
        }
        
    }
}
