﻿using ProBet.Core.Services;
using ProBet.Services;
using ProBet.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProBet.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : MetroWindow, INotificationService
    {
        #region Fields
        private readonly ILogService _logService;
        readonly object _notificationLockObject = new object();
        private bool _userConfirmedClose;
        Task<ProgressDialogController> _controller;
        #endregion

        public ShellView(ShellViewModel viewModel, ILogService logService)
        {
            _logService = logService;
            InitializeComponent();
            DataContext = viewModel;
            DialogManager.DialogOpened += DialogManager_DialogOpened;
            DialogManager.DialogClosed += DialogManager_DialogClosed;
        }

        private void DialogManager_DialogClosed(object sender, DialogStateChangedEventArgs e)
        {
            Task.Factory.StartNew(new Action(() =>
            {
                Thread.Sleep(500);
                Debug.WriteLine("Dialog opened: false");
            }));
        }
        private void DialogManager_DialogOpened(object sender, DialogStateChangedEventArgs e)
        {
            Debug.WriteLine("Dialog opened: true");
        }

        #region INotificationService
        private MetroWindow GetActiveWindow()
        {
            return this;
        }
        #endregion
        public Task<string> ShowInput(string details, string title = null)
        {
            lock (_notificationLockObject)
            {
                CloseProgress(null);

                //while (!AppView.Instance.IsDialogClosed)
                //{
                //    Thread.Sleep(1000);
                //}

                Task<string> inputTask = null;

                if (string.IsNullOrEmpty(title))
                {
                   // title = Strings.Information;
                }
                if (this.Dispatcher.CheckAccess())
                {
                    inputTask = this.ShowInputAsync(title, details);
                }
                else
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        inputTask = this.ShowInputAsync(title, details);
                    }));
                }
                return inputTask;
            }

        }
        public Task<MessageDialogResult> ShowMessage(string details, string title = null)
        {
            lock (_notificationLockObject)
            {
                CloseProgress(null);

                //while (!AppView.Instance.IsDialogClosed)
                //{
                //    Thread.Sleep(1000);
                //}

                Task<MessageDialogResult> result = null;
                if (string.IsNullOrEmpty(title))
                {
                 //   title = Strings.Information;
                }

                if (this.Dispatcher.CheckAccess())
                {
                    result = GetActiveWindow().ShowMessageAsync(title, details);
                }
                else
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {

                        result = GetActiveWindow().ShowMessageAsync(title, details);
                    }));
                }
                return result;
            }
        }
        public Task<MessageDialogResult> ShowAskMessage(string message, string title, MessageDialogStyle dialogStyle)
        {
            lock (_notificationLockObject)
            {
                CloseProgress(null);

                //while (!AppView.Instance.IsDialogClosed)
                //{
                //    Thread.Sleep(1000);
                //}

                Task<MessageDialogResult> result = null;


                if (string.IsNullOrEmpty(title))
                {
                  //  title = Strings.Information;
                }

                if (this.Dispatcher.CheckAccess())
                {
                    result = GetActiveWindow().ShowMessageAsync(title, message, dialogStyle, this.MetroDialogOptions);                    
                }
                else
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        result = GetActiveWindow().ShowMessageAsync(title, message, dialogStyle, this.MetroDialogOptions);
                    }));
                }
                return result;

            }
        }
        public Task<ProgressDialogController> ShowProgress(string progressText, string progressTitle = null, double? progress = null)
        {
            lock (_notificationLockObject)
            {
                CloseProgress(null);

                //while (!AppView.Instance.IsDialogClosed)
                //{
                //    Thread.Sleep(1000);
                //}


                if (string.IsNullOrEmpty(progressTitle))
                {
                   // progressTitle = Strings.Action;
                }

                if (this.CheckAccess())
                {

                    return _controller = GetActiveWindow().ShowProgressAsync(progressTitle, progressText);
                }
                else
                {
                    this.Dispatcher.Invoke(new Action(() =>
                    {

                        _controller = GetActiveWindow().ShowProgressAsync(progressTitle, progressText);
                        return;
                    }));
                }

                return _controller;
            }
        }
        public async void CloseProgress(ProgressDialogController controller)
        {
            if (controller == null)
            {
                if (_controller != null && _controller.IsCompleted)
                {
                    var ctrl = _controller.Result;
                    if (ctrl.IsOpen)
                    {
                        await ctrl.CloseAsync();
                    }
                    _controller = null;
                }
            }
            else
            {

                if (controller.IsOpen)
                {
                    await controller.CloseAsync();
                }
                if (_controller != null && _controller.IsCompleted)
                {
                    var ctrl = _controller?.Result;
                    if (ctrl == controller)
                    {
                        _controller = null;
                    }
                }
            }
        }

        
        private void FocusContent()
        {
         
        }

       
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Soon!");
        }
    }
}
