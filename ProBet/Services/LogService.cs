﻿using ProBet.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProBet.Services
{
    public class LogService : ILogService
    {

        private static XElement SerializeExceptionXml(Exception ex)
        {
            var xElement = new XElement("Exception");
            xElement.SetElementValue("Source", ex.Source);
            if (ex.TargetSite != null)
            {
                xElement.SetElementValue("TargetName", ex.TargetSite.Name);
            }
            xElement.SetElementValue("Date", DateTime.Now.ToLongDateString());
            xElement.SetElementValue("Time", DateTime.Now.ToLongTimeString());
            xElement.SetElementValue("Computer", Dns.GetHostName());
            xElement.SetElementValue("Error", ex.Message);
            xElement.SetElementValue("StackTrace", ex.StackTrace);

 

            if (ex.InnerException != null)
            {
                var xInnerElement = new XElement("Exceptions");
                xInnerElement.Add(SerializeExceptionXml(ex.InnerException));
                xElement.Add(xInnerElement);
            }
            return xElement;
        }

        public void WriteLog(string message)
        {
            if (message == null) { return; }
            try
            {
                string logfile;
                var document = GetDocument(out logfile);
                var xRoot = document.Root;
                var messagesElement = xRoot.Element("Messages");
                var xComment = new XComment(String.Format("Timestamp: {0} User: {1}", DateTime.Now, Environment.UserName));
                var xMessage = new XElement("Message", message);

                messagesElement.AddFirst(xComment, xMessage);
                document.Save(logfile);
            }
            catch { }
        }


        private XDocument GetDocument(out string filePath)
        {
            XDocument xDoc = null;
            filePath = null;
            try
            {
                filePath = GetFilePath();
                xDoc = XDocument.Load(filePath);
                if (xDoc.Element("LogContent") == null)
                {
                    throw new Exception("Invalid log file format");
                }
            }
            catch (Exception)
            {
                xDoc = new XDocument();

                var xRoot = new XElement("LogContent");
                xDoc.Add(xRoot);
                var xExceptions = new XElement("Exceptions");
                xRoot.Add(xExceptions);

                var xMessages = new XElement("Messages");
                xRoot.Add(xMessages);

            }
            return xDoc;
        }

        private string GetFilePath()
        {
            try
            {
                var logDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs");
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
                var logFile = Path.Combine(logDir, String.Format("log{0:yyyy-MM-dd}.xml", DateTime.Now));
                return logFile;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to create log file or directory", ex);
            }
        }

        public void WriteErrorLog(Exception objException)
        {
            //2016-06-07 uzkomentuota, kad nesaugotu exceptions, del pc performance
            //if (objException == null) { return; }

            //try
            //{
            //    #region createMessage

            //    string logFile;
            //    var xDoc = GetDocument(out logFile);
            //    var xRoot = xDoc.Root;
            //    var exceptionElement = xRoot.Element("Exceptions");
            //    var serializedException = SerializeExceptionXml(objException);

            //    var xComment = new XComment($"Timestamp: {DateTime.Now} User: {Environment.UserName}");

            //    exceptionElement.AddFirst(xComment, serializedException);
            //    xDoc.Save(logFile);

            //    #endregion
            //}
            //catch { }

        }

        public void WriteLine(string text)
        {
            if (text == null) throw new ArgumentNullException(nameof(text));
            try
            {
                string logFile;
                var xDoc = GetDocument(out logFile);
                var xRoot = xDoc.Root;

                var messagesElement = xRoot.Element("Messages");

                if (messagesElement != null)
                {
                    var element = new XElement("Message", text);
                    element.SetAttributeValue("Timestamp", DateTime.Now);
                    element.SetAttributeValue("User", Environment.UserName);
                    messagesElement.AddFirst(element);
                    xDoc.Save(logFile);
                }
            }
            catch { }
        }


        public string Serialize(Exception exception)
        {
            return SerializeExceptionXml(exception).ToString();
        }


    }
}
