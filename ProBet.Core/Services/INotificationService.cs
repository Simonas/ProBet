﻿using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;

namespace ProBet.Core.Services
{
    public interface INotificationService
    {
        Task<string> ShowInput(string details, string title = null);

        Task<MessageDialogResult> ShowMessage(string details, string title = null);

        Task<MessageDialogResult> ShowAskMessage(string message, string title, MessageDialogStyle dialogStyle);

        Task<ProgressDialogController> ShowProgress(string progressText, string progressTitle = null, double? progress = null);

        void CloseProgress(ProgressDialogController controller);

    }
}
