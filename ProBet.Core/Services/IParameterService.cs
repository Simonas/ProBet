﻿using ProBet.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Core.Services
{
    public interface IParameterService : IEnumerable<IParameter>
    {
        IParameter this[string key] { get; }
        IEnumerable<IParameter> GetDatabaseParameters(int tab);
    }

}
