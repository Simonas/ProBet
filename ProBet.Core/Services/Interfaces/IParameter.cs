﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Core.Services.Interfaces
{
    public static class EcuParameterServiceExtensions
    {
        public static IParameter Parameter(this IEnumerable<IParameter> parameters, string key)
        {
            if (parameters == null) throw new ArgumentNullException(nameof(parameters));
            if (key == null) throw new ArgumentNullException(nameof(key));
            return parameters.FirstOrDefault(x => !string.IsNullOrEmpty(x.Key) && x.Key.ToLower() == key.ToLower());
        }
    }

    public interface IParameter
    {
        int Id { get; }
           
        string Key { get; }

        string Value { get; set; }
       
        int Tab { get; set; }
    }
}
