﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ProBet.Core.Services
{
    public interface IMenuService
    {
        Menu GetMenu();
        MenuItem GetMenuItem(string headerTitle);
        void Add(MenuItem menuItem, int? index = null);
    }
}
