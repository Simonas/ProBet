﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Core.Services
{
    public interface ILogService
    {
        void WriteErrorLog(Exception exception);

        void WriteLine(string text);
    }
}
