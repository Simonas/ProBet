﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProBet.Core.Controls
{
    public class BaseView : HeaderedContentControl
    {
        public static readonly DependencyProperty CanCloseProperty = DependencyProperty.Register(
           "CanClose", typeof(bool), typeof(BaseView), new PropertyMetadata(true));

        public bool CanClose
        {
            get { return (bool)GetValue(CanCloseProperty); }
            set { SetValue(CanCloseProperty, value); }
        }

        #region ctor&dtor
        static BaseView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BaseView), new FrameworkPropertyMetadata(typeof(BaseView)));
        }

        public BaseView()
        {

            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Close, OnCloseExecute, OnCloseCanExecute));
            this.InputBindings.Add(new InputBinding(ApplicationCommands.Close, new KeyGesture(Key.F4, ModifierKeys.Alt)));
        }
        #endregion

        protected virtual void OnClosing(CanExecuteRoutedEventArgs e)
        {
        }

        protected virtual void OnClosed()
        {
        }

        public void Close()
        {
            if (Application.Current.Dispatcher.CheckAccess())
            {
                OnClose();
            }
            else
            {
                Application.Current.Dispatcher.Invoke(new Action(OnClose));
            }
        }

        #region Private members
        private void OnClose()
        {
            try
            {
                if (!CanClose)
                {
                    return;
                }

                var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();


                var views =
                    regionManager.Regions[RegionNames.MainRegion].Views.Where(x => x.GetType() == this.GetType())
                        .ToList();


                foreach (object t in views)
                {
                    regionManager.Regions[RegionNames.MainRegion].Remove(t);
                }


                var view = regionManager.Regions[RegionNames.MainRegion].Views.LastOrDefault();
                if (view != null)
                {
                    regionManager.Regions[RegionNames.MainRegion].Activate(view);
                }

            }

            catch (Exception ex)
            {

            }
            finally
            {
                OnClosed();

            }
        }

        private void OnCloseCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanClose;
            OnClosing(e);
        }

        private void OnCloseExecute(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            Close();
        }
        #endregion
    }
}
