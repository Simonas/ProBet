﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;

namespace ProBet.Core.Resources
{
    public class CoreModule : IModule
    {
        public const string DatabaseName = "Data.db"; //Main database file name

        public const string DatabaseParametersName = "probet.db"; //Parameters database file name    

        public void Initialize()
        {

        }
    }
}
