﻿using Microsoft.Practices.ServiceLocation;
using ProBet.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Core.MVVM
{
    public class BaseVM : INotifyPropertyChanged
    {
        protected void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                if (propertyName == null)
                {
                    propertyName = String.Empty;
                }

                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var expression = property.Body as MemberExpression;
            if (expression != null)
            {
                var member = expression.Member;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(member.Name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;



        #region Fields
        private INotificationService _notificationService;
        private ILogService _logService;
        #endregion

        protected BaseVM()
        {


        }

        protected INotificationService NotificationService
        {
            get
            {
                if (_notificationService == null)
                {
                    _notificationService = ServiceLocator.Current.GetInstance<INotificationService>();
                }

                return _notificationService;
            }
        }

        protected ILogService LogService
        {
            get
            {
                if (_logService == null)
                {
                    _logService = ServiceLocator.Current.GetInstance<ILogService>();
                }

                return _logService;
            }
        }
    }
}
