﻿using ProBet.Core.Services.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Communication.Models
{
    public sealed class Parameter : IParameter
    {
        public Parameter()
        {
            
        }

        public Parameter(IParameter parameter)
        {
            Tab = parameter.Tab;
            Value = parameter.Value;
        }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Key { get; set; }

        public string Value { get; set; }

        public int Tab { get; set; }
    }
}
