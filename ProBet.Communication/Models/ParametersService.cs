﻿using ProBet.Core.Services;
using ProBet.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProBet.Core.Models
{
    public class ParametersService : IParameterService, IEnumerable<IParameter>
    {
        #region Fields
        private readonly List<IParameter> _cacheList;
        #endregion

        #region ctor&dtor
        public ParametersService()
        {
            _cacheList = new List<IParameter>();

            using (var dbConnection = new SQLiteConnection("probet.db"))
            {
                _cacheList.AddRange(dbConnection.Table<EcuParameter>().ToList());
            }
        }
        #endregion
    }
}
