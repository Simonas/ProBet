﻿using ProBet.Communication.Models;
using ProBet.Core.Services;
using ProBet.Core.Services.Interfaces;
using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Communication.Modules
{
    public class ParameterService : IParameterService, IEnumerable<IParameter>
    {
        #region Fields
        private readonly List<IParameter> _cacheList;
        #endregion

        #region ctor&dtor
        public ParameterService()
        {
            _cacheList = new List<IParameter>();

            using (var dbConnection = new SQLiteConnection(Core.Resources.CoreModule.DatabaseParametersName))
            {
                _cacheList.AddRange(dbConnection.Table<Parameter>().ToList());
            }
        }
        #endregion

        #region Properties
        public IParameter this[string key]
        {
            get
            {
                if (key == null) throw new ArgumentNullException("key");

                var parameter = _cacheList.FirstOrDefault(x => !string.IsNullOrEmpty(x.Key) && x.Key.ToLower() == key.ToLower());
                if (parameter == null)
                {
                    throw new Exception("Parameter not found: " + key);
                }
                return parameter;
            }
        }

        #endregion

        #region Methods
        public IEnumerable<IParameter> GetDatabaseParameters(int tab)
        {
            var data = _cacheList.Where(x => x.Tab == tab).OrderBy(x => x.Id);

            // if (payload != null)
            // {
            // return data.Where(x => x.Payload == payload).OrderBy(x => x.Index);
            // }

            return data;
        }

        public IEnumerator<IParameter> GetEnumerator()
        {
            return _cacheList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        #endregion
    }
}
