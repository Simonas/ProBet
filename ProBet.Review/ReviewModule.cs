﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using ProBet.Core;
using ProBet.Review.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Review
{
    public class ReviewModule : IModule
    {
        #region Fields
        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _unityContainer;
        private readonly IEventAggregator _eventAggregator;
        #endregion

    
        #region ctor&dtor
        public ReviewModule(IRegionManager regionManager, IUnityContainer unityContainer, IEventAggregator eventAggregator)
        {
            _regionManager = regionManager;
            _unityContainer = unityContainer;
            _eventAggregator = eventAggregator;
        }
        #endregion

        #region Methods
        public void Initialize()
        {
            try
            {               
                ShowReviewView();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        private void ShowReviewView()
        {
            try
            {
                //if (_regionManager.Regions[RegionNames.MainRegion].ActiveViews.Any(x => x is ReviewView))
                //{
                //    return;
                //}
                //if (!_regionManager.Regions[RegionNames.MainRegion].Views.Contains(ReviewV))
                //{
                //    _regionManager.AddToRegion(RegionNames.MainRegion, ReviewV);
                //}
              
                //_regionManager.Regions[RegionNames.MainRegion].Activate(ReviewV);
            }
            catch { }
        }

    }
}
