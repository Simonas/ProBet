﻿using Microsoft.Practices.ServiceLocation;
using ProBet.Review.Resources;
using ProBet.Review.ViewModels;
using ProBet.Review.ViewModels.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProBet.Review.Views.Steps
{
    /// <summary>
    /// Interaction logic for OverView.xaml
    /// </summary>
    public partial class OverView : StepView
    {
        public OverView()
        {            
            InitializeComponent();
        }
        protected override StepVM OnCreateViewModel()
        {
            return ServiceLocator.Current.GetInstance<OverViewVM>();
        }
    }
}
