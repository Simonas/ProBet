﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProBet.Review.ViewModels.Steps
{
    public class OverViewVM : StepVM
    {
        #region ctor&dtor
        public OverViewVM()
        {
            base.Model = base.ParameterService.GetDatabaseParameters(1).ToList();
        }
        #endregion

        #region Properties

        public double Bankroll
        {
            get { return Convert.ToDouble(base.ParameterService["bankroll"].Value); }
        }

        public double FivePercent
        {
            get
            {
                return (5 * Bankroll / 100);
            }
        }

        public double ThreePercent
        {
            get
            {
                return (3 * Bankroll / 100);
            }
        }

        public double Wons
        {
            get { return 0; }
        }

        public double Losts
        {
            get { return 0; }
        }

        public double Equateds
        {
            get { return 0; }
        }

        public string WinPercentage
        {
            get { return string.Format("{0:N2} %", 1).Replace(",","."); }
        }

        public double Profit
        {
            get { return 0; }
        }
        #endregion
    }
}
