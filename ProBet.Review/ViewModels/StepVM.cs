﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using ProBet.Communication.Modules;
using ProBet.Core.MVVM;
using ProBet.Core.Services;
using ProBet.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ProBet.Review.ViewModels
{
    public abstract class StepVM : BaseVM
    {
        #region ctor&dtor
        protected StepVM()
        {
            _model = new List<IParameter>();

            EventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();

            ParameterService = new ParameterService();

            //ParameterService = ServiceLocator.Current.GetInstance<IParameterService>();

            InitializeCommand = new AsyncCommand(InitializeCommandExecute, InitializeCommandCanExecute);
            DeinitializeCommand = new AsyncCommand(DeinitializeCommandExecute, DeinitializeCommandCanExecute);
        }
        #endregion

        #region Properties      
        public IEventAggregator EventAggregator { get; }
        protected IParameterService ParameterService { get; }
        
        public IList<IParameter> Model
        {
            get { return _model; }
            set
            {
                if (value != null)
                {
                    _model.Clear();
                    _model.AddRange(value);
                }
            }
        }
        #endregion

        #region Fields
        private List<IParameter> _model;
        #endregion


        #region Methods
        protected virtual void OnDeinitialize()
        {

        }

        protected virtual void OnInitialize()
        {

        }
        #endregion

        #region Commands
        protected virtual bool InitializeCommandCanExecute(object arg)
        {
            return true;
        }
        private void InitializeCommandExecute(object obj)
        {
            try
            {
                //Do initialize
                OnInitialize();
            }
            catch (Exception ex)
            {
                //NotificationService.ShowMessage(
                NotificationService.ShowMessage("Nepavyko inicializuoti");
            }
            finally
            {
                OnPropertyChanged(string.Empty);
            }
        }

        protected virtual bool DeinitializeCommandCanExecute(object arg)
        {
            return true;
        }
        private void DeinitializeCommandExecute(object obj)
        {
            try
            {                             
                //Do deinitialize
                OnDeinitialize();
            }
            catch (Exception ex)
            {
                LogService.WriteErrorLog(ex);
            }
        }

        public ICommand DeinitializeCommand { get; private set; }
        public ICommand InitializeCommand { get; private set; }
        #endregion


    }
}
