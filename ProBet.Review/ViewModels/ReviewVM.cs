﻿using Microsoft.Practices.Prism.Regions;
using ProBet.Core.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ProBet.Review.ViewModels
{
    public class ReviewVM : BaseVM
    {
        private readonly IRegionManager _regionManager;


        public ReviewVM(IRegionManager regionManager)
        {
            _regionManager = regionManager;

            InitializeCommand = new DelegateCommand(InitializeCommandExecute, InitializeCommandCanExecute);
        }

        private bool InitializeCommandCanExecute(object obj)
        {
            return true;
        }

        private void InitializeCommandExecute(object obj)
        {
            
        }


        public ICommand InitializeCommand { get; private set; }
    }
}
