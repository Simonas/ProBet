﻿using MahApps.Metro.Controls;
using ProBet.Review.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ProBet.Review.Resources
{
    public class StepView : ContentControl
    {
        #region ctor&dtor
        public StepView()
        {
            this.Loaded += StepView_Loaded;
            this.Unloaded += StepView_Unloaded;

            if (ViewModel == null)
            {
                ViewModel = OnCreateViewModel();
            }

            if (ViewModel != null && (DataContext == null || DataContext != ViewModel))
            {
                DataContext = ViewModel;
            }
        }

        static StepView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(StepView), new FrameworkPropertyMetadata(typeof(StepView)));
        }
        #endregion

        #region Properties
        public StepVM ViewModel { get; private set; }
        #endregion

        #region Methods
        protected virtual StepVM OnCreateViewModel()
        {
            return null;
        }

        void StepView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.SizeChanged += StepView_SizeChanged;

            var viewModel = DataContext as StepVM;

            if (viewModel != null)
            {
                if (viewModel.InitializeCommand.CanExecute(null))
                {
                    viewModel.InitializeCommand.Execute(null);
                }
            }
        }

        void StepView_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.SizeChanged -= StepView_SizeChanged;
            if (ViewModel != null)
            {
                if (ViewModel.DeinitializeCommand.CanExecute(null))
                {
                    ViewModel.DeinitializeCommand.Execute(null);
                }
            }

            OnUnloaded();
        }

        protected virtual void OnUnloaded()
        {

        }

        private void StepView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }      
        }
        #endregion

    }
}
